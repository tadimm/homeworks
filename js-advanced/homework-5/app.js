(async function getList() {
  const responseUsers = await fetch(
    "https://ajax.test-danit.com/api/json/users",
    { method: "GET" }
  );
  const responsePosts = await fetch(
    "https://ajax.test-danit.com/api/json/posts",
    { method: "GET" }
  );
  const users = await responseUsers.json();
  const posts = await responsePosts.json();
  const fragment = new DocumentFragment();

  for (let user of users) {
    const person = new User(user, posts);
    for (let post of person.posts) {
      const card = new Card(person, post);
      fragment.append(card.render());
    }
  }
  document.body.append(fragment);
})();

class User {
  constructor({ id, name, userName, email }, postsArr) {
    this.id = id;
    this.name = name;
    this.userName = userName;
    this.email = email;
    this.date = new Date();
    this.posts = this.getPosts(postsArr);
  }
  getPosts(arr) {
    return arr.filter(({ userId }) => userId === this.id);
  }
}
class Card {
  constructor({ name, userName, email, date }, { id, title, body }) {
    this.id = id;
    this.name = name;
    this.userName = userName;
    this.email = email;
    this.title = title;
    this.body = body;
    this.date = date;
    this.buttonHandler = buttonHandler.bind(this);
  }
  render() {
    const defaultSrc =
      "https://i.pinimg.com/474x/4c/3e/3b/4c3e3b91f05a5765aa544ac7557d6642.jpg";
    const container = createElement("div", "card", null, null, null, this.id);
    const profileImg = createElement(
      "img",
      "card__image",
      null,
      defaultSrc,
      "profile image"
    );
    const cardContent = createElement("div", ["card__content", "content"]);
    const fullName = createElement("span", "content__fullname", this.name);
    const email = createElement("span", "content__email", this.email);
    const time = createElement("span", "content__time", this.date);
    const title = createElement("p", "content__title", this.title);
    const text = createElement("p", "content__text", this.body);
    const deleteBtn = createElement("button", "button__delete", "Delete Post");

    deleteBtn.addEventListener("click", this.buttonHandler);

    cardContent.append(fullName, email, time, title, text);
    container.append(profileImg, cardContent, deleteBtn);
    return container;
  }
}
function createElement(
  tagName,
  className,
  textContent,
  src,
  alt = "image",
  id
) {
  const element = document.createElement(tagName);
  if (className) {
    if (typeof className === "string") {
      element.classList.add(className);
    } else if (Array.isArray(className)) {
      element.classList.add(...className);
    }
  }
  if (textContent) {
    element.textContent = textContent;
  }
  if (src) {
    element.src = src;
    element.alt = alt;
  }
  if (id) {
    element.id = id;
  }
  return element;
}

async function buttonHandler(ev) {
  const parent = ev.target.closest(".card");
  const postId = parent.id;
  if (ev.target.classList.contains("button__delete")) {
    const response = await fetch(
      `https://ajax.test-danit.com/api/json/posts/${postId}`,
      {
        method: "DELETE",
      }
    );
    if (response.ok) {
      parent.remove();
    } else {
      throw new Error("Failed to delete post from server");
    }
  }
}
