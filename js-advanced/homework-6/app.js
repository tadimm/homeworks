// Написати програму "Я тебе знайду по IP"
// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const button = document.querySelector(".finder");
const loader = document.querySelector(".loader");
button.addEventListener("click", buttonHandler);

async function buttonHandler() {
  const responseIp = await fetch("http://api.ipify.org/?format=json", {
    method: "GET",
  });
  const ip = await responseIp.json();

  showLoader();

  if (responseIp.ok) {
    const responseInfo = await fetch(
      "http://ip-api.com/json/?fields=continent,country,regionName,city,district",
      {
        method: "GET",
      }
    );
    const info = await responseInfo.json();

    hideLoader();

    const fullInfo = `Continent: ${info.continent}<br> 
    Country: ${info.country}<br>
    Region: ${info.regionName}<br>
    City: ${info.city}<br>
    District: ${info.district}`;
    document.querySelector(".info-here").innerHTML = fullInfo;
  }
}

function showLoader() {
  loader.classList.remove("hidden");
}

function hideLoader() {
  loader.classList.add("hidden");
}
