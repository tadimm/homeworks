class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }
  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return super.salary * 3;
  }
}
const programmer1 = new Programmer("Nastia", 25, 3000, "JavaScript");
const programmer2 = new Programmer("Jon", 35, 5000, "Python");
const programmer3 = new Programmer("Monika", 30, 4000, "C#");
console.log(programmer1, programmer1.salary);
console.log(programmer2, programmer2.salary);
console.log(programmer3, programmer3.salary);
