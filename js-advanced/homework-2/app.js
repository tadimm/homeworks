const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

checkObject(books);

function generateElement(tag, className, content) {
  const element = document.createElement(tag);
  element.classList.add(className);
  if (content) element.textContent = content;
  return element;
}

function checkObject(arr) {
  const div = document.getElementById("root");
  const ul = generateElement("ul", "list");
  const keys = ["author", "name", "price"];
  arr.forEach((obj) => {
    try {
      for (let key of keys) {
        if (!Object.keys(obj).includes(key))
          throw new Error(`There is no ${key} in this object`);
      }
      const li = generateElement("li", "list__item");
      for (let key in obj) {
        const p = generateElement("p", "list__text", `${key}: ${obj[key]}`);
        li.append(p);
      }
      ul.append(li);
    } catch (error) {
      console.log(error);
    }
  });
  div.append(ul);
}
