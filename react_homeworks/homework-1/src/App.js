import React, { useEffect } from "react";
import Modal from "./components/modal";
import Header from "./components/header";
import Footer from "./components/Footer";
import AppRoutes from "./AppRoutes";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts } from "./redux/products/actionCreators";
import { fetchCartProducts } from "./redux/cart/actionCreators";
import { fetchFavoriteProducts } from "./redux/favorites/actionCreators";

export const CART_LS_KEY = "cart";
export const FAVORITE_LS_KEY = "favorites";

const App = () => {
  const dispatch = useDispatch();
  const isModalShown = useSelector((state) => state.modal.isModalOpen);

  useEffect(() => {
    dispatch(fetchProducts());
    dispatch(fetchCartProducts());
    dispatch(fetchFavoriteProducts());
  }, []);

  return (
    <>
      <Header />
      <AppRoutes />
      {isModalShown && <Modal />}
      <Footer />
    </>
  );
};

export default App;
