import React from "react";
import ProductList from "../../components/product/list";
import styles from "./HomePage.module.css";

const HomePage = () => {
  return (
    <div className={styles.container}>
      <ProductList />
    </div>
  );
};

export default HomePage;
