import React from "react";
import styles from "./CartPage.module.css";
import { shallowEqual, useSelector } from "react-redux";
import CheckoutForm from "../../components/form/CheckoutForm";
import CartProduct from "../../components/cartProduct/CartProduct";

const CartPage = () => {
  const cart = useSelector((state) => state.cart.products, shallowEqual);
  return (
    <div className={styles.container}>
      <ul className={styles.cartList}>
        {cart?.map((product) => (
          <li key={product.code} className={styles.cartItem}>
            <CartProduct product={product} />
          </li>
        ))}
      </ul>
      <CheckoutForm />
    </div>
  );
};

export default CartPage;
