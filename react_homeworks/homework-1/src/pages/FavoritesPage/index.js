import React from "react";
import ProductCard from "../../components/product/card/ProductCard";
import styles from "./FavoritesPage.module.css";
import { shallowEqual, useSelector } from "react-redux";

const FavoritesPage = () => {
  const favorites = useSelector(
    (state) => state.favorites.products,
    shallowEqual
  );
  return (
    <ul className={styles.favList}>
      {favorites.map((product) => (
        <li key={product.code}>
          <ProductCard product={product} />
        </li>
      ))}
    </ul>
  );
};

export default FavoritesPage;
