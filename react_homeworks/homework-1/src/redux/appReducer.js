import { combineReducers } from "redux";
import productsReducer from "./products/reducer";
import cartReducer from "./cart/reducer";
import favoritesReducer from "./favorites/reducer";
import modalReducer from "./modal/reducer";

const appReducer = combineReducers({
  products: productsReducer,
  cart: cartReducer,
  favorites: favoritesReducer,
  modal: modalReducer,
});
export default appReducer;
