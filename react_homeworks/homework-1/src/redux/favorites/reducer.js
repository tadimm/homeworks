import { FAVORITE_LS_KEY } from "../../App";
import {
  clearLocalStorage,
  saveStateToLocalStorage,
} from "../../utils/localStorageHelper";
import { TOGGLE_FAVORITE_ITEM, SET_FAVORITES } from "./actions";

const initialState = {
  products: [],
};

const favoritesReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_FAVORITES: {
      return { ...state, products: action.payload };
    }

    case TOGGLE_FAVORITE_ITEM: {
      const copy = [...state.products];
      const product = action.payload;
      const index = copy.findIndex((el) => el.code === product.code);
      if (index !== -1) {
        copy.splice(index, 1);

        if (!copy.length) clearLocalStorage(FAVORITE_LS_KEY);
        else saveStateToLocalStorage(FAVORITE_LS_KEY, copy);

        return { ...state, products: copy };
      } else {
        saveStateToLocalStorage(FAVORITE_LS_KEY, [product, ...state.products]);
        return { ...state, products: [product, ...state.products] };
      }
    }
    default:
      return state;
  }
};

export default favoritesReducer;
