import { TOGGLE_FAVORITE_ITEM, SET_FAVORITES } from "./actions";
import { getStateFromLocalStorage } from "../../utils/localStorageHelper";
import { FAVORITE_LS_KEY } from "../../App";

export const setFavorites = (value) => ({
  type: SET_FAVORITES,
  payload: value,
});

export const toggleFavoriteItem = (value) => ({
  type: TOGGLE_FAVORITE_ITEM,
  payload: value,
});

export const fetchFavoriteProducts = () => async (dispatch) => {
  try {
    const favoriteFromLs = getStateFromLocalStorage(FAVORITE_LS_KEY);
    if (favoriteFromLs) {
      dispatch(setFavorites(favoriteFromLs));
    }
  } catch (error) {
    console.error("Error in getting cards:", error);
  }
};
