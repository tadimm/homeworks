import {
  SET_MODAL,
  SET_MODAL_FUNCTION,
  SET_MODAL_HEADER,
  SET_MODAL_TEXT,
} from "./actions";

const initialState = {
  isModalOpen: false,
  header: "",
  text: "",
  function: () => {},
};

const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MODAL: {
      return { ...state, isModalOpen: action.payload };
    }
    case SET_MODAL_HEADER: {
      return { ...state, header: action.payload };
    }
    case SET_MODAL_TEXT: {
      return { ...state, text: action.payload };
    }
    case SET_MODAL_FUNCTION: {
      return { ...state, function: action.payload };
    }
    default:
      return state;
  }
};

export default modalReducer;
