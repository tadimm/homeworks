import {
  SET_MODAL,
  SET_MODAL_FUNCTION,
  SET_MODAL_HEADER,
  SET_MODAL_TEXT,
} from "./actions";

export const setModal = (value) => ({
  type: SET_MODAL,
  payload: value,
});

export const setModalHeader = (value) => ({
  type: SET_MODAL_HEADER,
  payload: value,
});

export const setModalText = (value) => ({
  type: SET_MODAL_TEXT,
  payload: value,
});

export const setModalFunction = (value) => ({
  type: SET_MODAL_FUNCTION,
  payload: value,
});
