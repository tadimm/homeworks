import { ADD_TO_CART, CLEAR_CART, REMOVE_FROM_CART, SET_CART } from "./actions";
import { getStateFromLocalStorage } from "../../utils/localStorageHelper";
import { CART_LS_KEY } from "../../App";

export const setCart = (value) => ({ type: SET_CART, payload: value });

export const addToCart = (value) => ({ type: ADD_TO_CART, payload: value });

export const clearCart = (value) => ({ type: CLEAR_CART, payload: value });

export const removeFromCart = (value) => ({
  type: REMOVE_FROM_CART,
  payload: value,
});

export const fetchCartProducts = () => async (dispatch) => {
  try {
    const cartFromLs = getStateFromLocalStorage(CART_LS_KEY);
    if (cartFromLs) {
      dispatch(setCart(cartFromLs));
    }
  } catch (error) {
    console.error("Error in getting cards:", error);
  }
};
