import { CART_LS_KEY } from "../../App";
import {
  clearLocalStorage,
  saveStateToLocalStorage,
} from "../../utils/localStorageHelper";
import { ADD_TO_CART, CLEAR_CART, REMOVE_FROM_CART, SET_CART } from "./actions";

const initialState = {
  products: [],
  amount: 0,
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CART: {
      for (let i = 0; i < action.payload.length; i++) {
        state.amount += action.payload[i].count;
      }
      return { ...state, products: action.payload, amount: state.amount };
    }
    case ADD_TO_CART: {
      const copyState = [...state.products];
      const currentProduct = action.payload;
      const item = copyState.find(
        (element) => element.code === currentProduct.code
      );
      const index = copyState.findIndex(
        (element) => element.code === currentProduct.code
      );

      if (item) {
        copyState[index] = { ...item, count: item.count + 1 };
        state.amount++;
        saveStateToLocalStorage(CART_LS_KEY, copyState);
        return { ...state, products: copyState, amount: state.amount };
      } else {
        const newState = [{ ...currentProduct, count: 1 }, ...state.products];
        state.amount++;
        saveStateToLocalStorage(CART_LS_KEY, newState);
        return { ...state, products: newState, amount: state.amount };
      }
    }
    case REMOVE_FROM_CART: {
      const copy = [...state.products];
      const currentProduct = action.payload;
      const item = copy.find((element) => element.code === currentProduct.code);
      const index = copy.findIndex((item) => item.code === currentProduct.code);

      copy[index] = { ...item, count: item.count - 1 };
      state.amount--;
      if (!copy[index].count) {
        copy.splice(index, 1);
      }
      if (!copy.length) {
        clearLocalStorage(CART_LS_KEY);
      } else {
        saveStateToLocalStorage(CART_LS_KEY, copy);
      }
      return { ...state, products: copy, amount: state.amount };
    }
    case CLEAR_CART: {
      return { ...state, products: [], amount: 0 };
    }
    default:
      return state;
  }
};

export default cartReducer;
