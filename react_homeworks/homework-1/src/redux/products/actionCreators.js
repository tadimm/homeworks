import axios from "axios";
import { SET_PRODUCTS } from "./actions";

export const setProducts = (value) => ({ type: SET_PRODUCTS, payload: value });

export const fetchProducts = () => async (dispatch) => {
  try {
    const response = await axios("products.json");
    dispatch(setProducts(response.data));
  } catch (error) {
    console.error("Error in getting cards:", error);
  }
};
