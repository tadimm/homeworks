import React from "react";
import styles from "./Cart.module.css";
import { useSelector } from "react-redux";

const Cart = () => {
  const amount = useSelector((state) => state.cart.amount);

  return (
    <div className={styles.cart}>
      <span className={styles.cartName}>Cart</span>
      <div className={styles.amount}>
        <span className={styles.cartAmount}>{amount}</span>
      </div>
    </div>
  );
};

export default Cart;
