import React from "react";
import styles from "./Action.module.css";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { setModal } from "../../redux/modal/actionCreators";

const Action = () => {
  const dispatch = useDispatch();
  const actionCallback = useSelector(
    (state) => state.modal.function,
    shallowEqual
  );

  const handleCloseModal = () => {
    dispatch(setModal(false));
  };

  const handleConfirmModal = () => {
    actionCallback();
    handleCloseModal();
  };

  return (
    <>
      <button
        className={styles.modalBtns}
        style={{ "--color": "#7fe07f" }}
        onClick={handleConfirmModal}
      >
        Ok
      </button>
      <button
        className={styles.modalBtns}
        style={{ "--color": "#ff7070" }}
        onClick={handleCloseModal}
      >
        Cancel
      </button>
    </>
  );
};

export default Action;
