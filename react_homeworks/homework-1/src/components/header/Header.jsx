import React from "react";
import styles from "./Header.module.css";
import Cart from "../cart/Cart";
import Favorite from "../favorite/Favorite";
import { NavLink } from "react-router-dom";

const Header = () => {
  return (
    <header className={styles.header}>
      <NavLink to="/" className={styles.logo}>
        Slime Fans
      </NavLink>
      <nav className={styles.box}>
        <ul className={styles.list}>
          <li>
            <NavLink to="/favorites" className={styles.headerLink}>
              <Favorite />
            </NavLink>
          </li>
          <li>
            <NavLink to="/cart" className={styles.headerLink}>
              <Cart />
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
