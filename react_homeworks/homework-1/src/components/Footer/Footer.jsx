import React from "react";
import styles from "./Footer.module.css";

const Footer = () => {
  return (
    <footer>
      <p className={styles.footerText}>We are here for all slime fans</p>
    </footer>
  );
};

export default Footer;
