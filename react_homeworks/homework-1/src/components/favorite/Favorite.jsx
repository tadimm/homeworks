import React from "react";
import styles from "./Favorite.module.css";
import { useSelector } from "react-redux";

const Favorite = () => {
  const favorites = useSelector((state) => state.favorites.products);
  return (
    <div className={styles.favorite}>
      <span className={styles.favoriteName}>Favorite</span>
      <div className={styles.amount}>
        <span className={styles.favoriteAmount}>{favorites.length}</span>
      </div>
    </div>
  );
};

export default Favorite;
