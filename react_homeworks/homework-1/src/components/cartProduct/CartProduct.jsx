import React from "react";
import { useDispatch } from "react-redux";
import { addToCart, removeFromCart } from "../../redux/cart/actionCreators";
import PropTypes from "prop-types";
import styles from "./CartProduct.module.css";
import { AiOutlineMinus, AiOutlinePlus } from "react-icons/ai";

const CartProduct = ({ product }) => {
  const dispatch = useDispatch();

  const plusCount = () => {
    dispatch(addToCart(product));
  };

  const minusCount = () => {
    dispatch(removeFromCart(product));
  };

  return (
    <>
      <div className={styles.card}>
        <img
          className={styles.cardImage}
          src={product.picture}
          alt={product.name}
        />
        <div className={styles.infoContainer}>
          <p className={styles.name}>{product.name}</p>
          <p className={styles.description}>{product.description}</p>
          <p className={styles.price}>{product.price}</p>
          <div className={styles.counterContainer}>
            <button className={styles.decrement} onClick={minusCount}>
              <AiOutlineMinus size={20} />
            </button>
            <span className={styles.amount}>{product.count}</span>
            <button className={styles.increment} onClick={plusCount}>
              <AiOutlinePlus size={20} />
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

CartProduct.propTypes = {
  product: PropTypes.object,
};

CartProduct.defaultProps = {
  product: {},
};

export default CartProduct;
