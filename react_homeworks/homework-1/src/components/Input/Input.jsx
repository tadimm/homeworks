import { useField } from "formik";
import React from "react";
import styles from "./Input.module.css";

const Input = ({ ...props }) => {
  const [field, meta] = useField(props);
  return (
    <div className={styles.root}>
      <input className={styles.textInput} {...field} {...props} />
      {meta.touched && meta.error ? (
        <div className={styles.error}>{meta.error}</div>
      ) : null}
    </div>
  );
};

export default Input;
