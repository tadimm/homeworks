import { useField } from "formik";
import React from "react";
import styles from "./Textarea.module.css";

const Textarea = ({ ...props }) => {
  const [field, meta, helpers] = useField(props);

  const handleBlur = () => {
    helpers.setTouched(true);
  };

  return (
    <div>
      <textarea
        className={styles.textarea}
        {...field}
        {...props}
        onBlur={handleBlur}
      />
      {meta.touched && meta.error ? (
        <div className={styles.error}>{meta.error}</div>
      ) : null}
    </div>
  );
};

export default Textarea;
