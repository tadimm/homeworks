import React, { useState, useEffect } from "react";
import styles from "./ProductCard.module.css";
import PropTypes from "prop-types";
import { BsFillHeartFill, BsHeart } from "react-icons/bs";
import { useDispatch, useSelector } from "react-redux";
import { toggleFavoriteItem } from "../../../redux/favorites/actionCreators";
import {
  setModal,
  setModalFunction,
  setModalHeader,
  setModalText,
} from "../../../redux/modal/actionCreators";
import { addToCart } from "../../../redux/cart/actionCreators";

const ProductCard = ({ product }) => {
  const [isFavorite, setIsFavorite] = useState(false);

  const dispatch = useDispatch();

  const favorites = useSelector((state) => state.favorites.products);

  const handleAddItem = () => {
    dispatch(setModalFunction(() => dispatch(addToCart(product))));
    dispatch(
      setModalHeader("Are you sure you want to add this product to the cart?")
    );
    dispatch(setModalText("This product will be added to the cart"));
    dispatch(setModal(true));
  };

  const handleFavoriteBtnClick = () => {
    dispatch(toggleFavoriteItem(product));

    setIsFavorite((prev) => !prev);
  };

  useEffect(() => {
    const isProductFavorite = favorites?.find(
      (item) => item.code === product.code
    );
    setIsFavorite(!!isProductFavorite);
  }, []);

  return (
    <>
      <div className={styles.card}>
        <img
          className={styles.cardImage}
          src={product.picture}
          alt={product.name}
        />
        <p className={styles.name}>{product.name}</p>
        <p className={styles.price}>{product.price}</p>
        <button className={styles.favBtn} onClick={handleFavoriteBtnClick}>
          {isFavorite ? (
            <BsFillHeartFill color={"#ffa0db"} size={24} />
          ) : (
            <BsHeart color={"grey"} size={24} />
          )}
        </button>
        <button className={styles.cardBtn} onClick={handleAddItem}>
          Add to cart
        </button>
      </div>
    </>
  );
};

ProductCard.propTypes = {
  product: PropTypes.object,
};

ProductCard.defaultProps = {
  product: {},
};

export default ProductCard;
