import React from "react";
import ProductCard from "../card/ProductCard";
import styles from "./ProductList.module.css";
import { shallowEqual, useSelector } from "react-redux";

const ProductList = () => {
  const products = useSelector(
    (state) => state.products.products,
    shallowEqual
  );
  return (
    <ul className={styles.container}>
      {products.map((product) => (
        <li key={product.code}>
          <ProductCard product={product} />
        </li>
      ))}
    </ul>
  );
};

export default ProductList;
