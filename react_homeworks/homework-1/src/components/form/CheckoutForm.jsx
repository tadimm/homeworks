import React from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import Input from "../Input/Input";
import styles from "./CheckoutForm.module.css";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { clearCart } from "../../redux/cart/actionCreators";
import { clearLocalStorage } from "../../utils/localStorageHelper";
import { CART_LS_KEY } from "../../App";
import Textarea from "../Textarea/Textarea";
import { PatternFormat } from "react-number-format";
import { useState } from "react";

const CheckoutForm = () => {
  const dispatch = useDispatch();
  const cart = useSelector((state) => state.cart.products, shallowEqual);
  const [val, setVal] = useState("123456789");

  const onSubmit = (values, actions) => {
    console.log(values);
    console.log(cart);
    dispatch(clearCart());
    clearLocalStorage(CART_LS_KEY);
    actions.resetForm();
  };

  return (
    <>
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          age: "",
          deliveryAdress: "",
          phoneNumber: "",
        }}
        validationSchema={Yup.object({
          firstName: Yup.string()
            .max(15, "Should be 15 characters or less")
            .required("Required"),
          lastName: Yup.string()
            .max(25, "Should be 25 characters or less")
            .required("Required"),
          age: Yup.number()
            .max(110, "Maximum age can be 110 years old")
            .min(18, "Get our slimes if you're over 18 years old")
            .required("Required"),
          deliveryAdress: Yup.string()
            .min(20, "This field should contain 20 characters or more")
            .required("Required"),
          phoneNumber: Yup.number().required("Required"),
        })}
        onSubmit={onSubmit}
      >
        <Form className={styles.root}>
          <h2 className={styles.formTitle}>Checkout Form</h2>
          <Input name="firstName" type="text" placeholder="First Name" />
          <Input name="lastName" type="text" placeholder="Last Name" />
          <Input name="age" type="text" placeholder="Your Age" />
          <Textarea
            name="deliveryAdress"
            type="text"
            placeholder="Delivery Adress"
          />
          <PatternFormat
            format="+38 (###) ###-####"
            name="phoneNumber"
            mask="_"
            allowEmptyFormatting
            onValueChange={(values) => {
              setVal(values.value);
            }}
            style={{
              padding: "10px",
              fontSize: "16px",
              marginBottom: "10px",
              color: "#887c7c",
            }}
          />
          <button type="submit" className={styles.submit}>
            Checkout
          </button>
        </Form>
      </Formik>
    </>
  );
};
export default CheckoutForm;
