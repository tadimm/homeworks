import React from "react";
import styles from "./Modal.module.css";
import { useDispatch, useSelector } from "react-redux";
import Action from "../actions";
import { setModal } from "../../redux/modal/actionCreators";

const Modal = () => {
  const dispatch = useDispatch();
  const header = useSelector((state) => state.modal.header);
  const text = useSelector((state) => state.modal.text);

  const handleModalOnShadowClick = (ev) => {
    ev.target === ev.currentTarget && dispatch(setModal(false));
  };

  return (
    <div className={styles.modalShadow} onClick={handleModalOnShadowClick}>
      <div className={styles.container}>
        <div className={styles.headerWrapper}>
          <p className={styles.title}>{header}</p>
        </div>
        <p className={styles.mainText}>{text}</p>
        <div className={styles.btnsWrapper}>
          <Action />
        </div>
      </div>
    </div>
  );
};

export default Modal;
