import React from "react";
import { Routes, Route } from "react-router-dom";
import CartPage from "./pages/CartPage";
import FavoritesPage from "./pages/FavoritesPage";
import HomePage from "./pages/HomePage";

function AppRoutes() {
  return (
    <Routes>
      <Route path="/" element={<HomePage />}></Route>
      <Route path="/cart" element={<CartPage />}></Route>
      <Route path="/favorites" element={<FavoritesPage />}></Route>
    </Routes>
  );
}

export default AppRoutes;
