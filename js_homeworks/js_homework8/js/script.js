//1 Опишіть своїми словами що таке Document Object Model (DOM)
// Це об'єктна модель документа, яку створює браузер у пам'яті комп'ютера
// на основі HTML кода. HTML - це текст і з ним неможливо працювати
// як є. Для цього його треба розібрати і створити нна його основі об'єкт, 
// це робить браузер, цим об'єктом і є DOM.
//2 Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerHTML вилучає весь контент разом з тегами із вказаного елемента на HTML сторінці.
// innerText вилучає весь контент без тегів із вказаного елемента на HTML сторінці.
// Також, якщо ми введемо такий приклад:
//const element = document.querySelector('optionsList');
//element.innerHTML = '<b>kkk</b>'; - тег <b> стає тег елемонтом і на екрані ми бачимо жирні kkk;
//element.innerText = '<b<kkk</b>'; - в цьму випадку ми побачимо весь текст на крані разом з тегами
//3 Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Ці два вважаються кращими зараз: document.querySelector() | document.querySelectorAll()
// у дужках ми пишемо елемент, який хочемо знайти. Якщо ми хочемо знайти
// за id, тоді потрібно поставити '#' перед назвою. Якщо за класом, тоді '.'.
// Також є: getElementById(), getElementsByName(), getElementsByTagName(),
// getElementsByClassName().

//1
const paragraphs = document.querySelectorAll('p');
paragraphs.forEach((paragraph) => {
    paragraph.style.background = "#ff0000";
});

//2
const list = document.querySelector('#optionsList');
console.log(list);
const parent = list.parentElement;
console.log(parent);
const children = list.childNodes;
console.log(children);
children.forEach((child) => {
    console.log(typeof child);
});

//3
const test = document.querySelector('#testParagraph');
test.textContent = 'This is a paragraph';

//4-5
const header = document.querySelector('.main-header');
const headerChildren = [...header.children];
console.log(headerChildren);
headerChildren.forEach((child) => {
    child.classList.add('nav-item');
});
console.log(headerChildren);

//6
const titles = document.querySelectorAll('.section-title');
titles.forEach((title) => {
    title.classList.remove('section-title');
});
console.log(titles);