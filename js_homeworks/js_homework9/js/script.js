//1 Опишіть, як можна створити новий HTML тег на сторінці.
// за допомогою document.createElement(), в дужках у лапках пишемо тег. Щоб він з'явився на сторінці треба його додати
// за допомогою append(), він буде добавлений в кінець HTML.
// insertAdjacentElement() з двома параметрами. У першому параметрі ми ставимо: 'beforebegin', 'afterbegin', 'beforeend', 'afterend'.
// другим параметром є елемент.

//2 Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// перший параметр - це позиція, куди буде вставлено елемент
// 'beforebegin' - до початку опорного елемента, тобто перед ним.
// 'afterbegin' - після початку опорного елемента.
// 'beforeend' - на прикінці опорного елемента.
// 'afterend' - після опорного елемента.

//3 Як можна видалити елемент зі сторінки?
// за допомогою element.remove()

const myArr = ["hello", "world", "Kyiv", "Kharkiv", "Odesa", "Lviv"];

function createList (arr, parent = document.body) {
const ul = document.createElement('ul');
arr.forEach((element) => {
    const li = document.createElement('li');
    li.innerHTML = `<h3>${element}</h3>`;
    ul.append(li);
});
parent.append(ul);
}
createList(myArr);
ul.insertAdjacentHTML()
