const btnArr = document.querySelectorAll(".btn");
const dataArr = [];

for (btn of btnArr) {
  dataArr.push(btn.dataset.key);
}

document.addEventListener("keydown", (ev) => {
  const pressedKey = ev.key.toLowerCase();
  if (dataArr.includes(pressedKey)) {
    const currentIndex = dataArr.indexOf(pressedKey);
    btnArr.forEach((btn) => {
      btn.style.backgroundColor = "black";
    });

    btnArr[currentIndex].style.backgroundColor = "blue";
  }
});
