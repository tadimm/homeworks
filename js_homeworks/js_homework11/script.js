const eyes = document.querySelectorAll(".fas");
for (let i = 0; i < eyes.length; i++) {
  eyes[i].addEventListener("click", (ev) => {
    ev.target.classList.toggle("fa-eye-slash");
    const input = ev.target.previousElementSibling;
    if (input.type === "password") {
      input.type = "text";
    } else {
      input.type = "password";
    }
  });
}

const form = document.querySelector("form");
const error = document.querySelector(".error");
form.addEventListener("submit", (ev) => {
  ev.preventDefault();
  if (form.password.value === form.confirmPassword.value) {
    alert("You are welcome");
  } else {
    error.textContent = "You should enter same password in these two lines";
  }
});
