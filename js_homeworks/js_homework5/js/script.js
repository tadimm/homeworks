//1 Що таке метод об'єкту?
// Це функція, яка належитьь об'єкту.
//2 Який тип даних може мати значення властивості об'єкта?
// Зазвичай це примітивний тип даних: рядок, число, Boolean, null, але може бути і інший об'єкт
//3 Об'єкт це посилальний тип даних. Що означає це поняття?
// Посилальний тип - це особливий посередницький внутрішний тип,
// який використовується з метою передачі інформації від крапки до дужок виклику.
function createNewUser() {
    const firstName = prompt('Enter your first name');
    const lastName = prompt('enter your last name');
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin(){
            return `${this.firstName[0]}${this.lastName}`.toLowerCase();
        }
    };
    return newUser;
}
const user1 = createNewUser();
console.log(user1.getLogin());