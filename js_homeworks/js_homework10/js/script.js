const tabs = document.querySelector(".tabs");
tabs.addEventListener("click", (ev) => {
  const active = document.querySelector(".active");
  active.classList.remove("active");
  ev.target.classList.add("active");

  const oldText = document.querySelector(".tabs-text-active");
  oldText.classList.remove("tabs-text-active");
  const data = ev.target.dataset.tab;
  const text = document.querySelector(`.tabs-content [data-tab=${data}]`);
  text.classList.add("tabs-text-active");
});
