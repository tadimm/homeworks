//1  Що таке екранування і навіщо воно потрібно в мовах програмування?
// Коли, наприклад в строці нам потрібно використати і апостроф
// і лапки, і щоб при цьому не було помилки у коді. В таких випадках
// нам треба використати екранування "\".
// Наприклад: 
// const text = "I'm reading a book \"To kill a mocking bird\"";

//2 Які засоби оголошення функції ви знаєте?
// function declaration
// function expression i named function expression

//3 Що таке hoisting, як він працює для змінних та функцій?
// Це механізм в JavaScript, в якому змінні і оголошення функцій пересуваються
// догори своєї області видимості перед тим, як код буде виконано.
// Це означає, що зовсім неважливо де були оголошені функції або змінні,
// всі вони пересуваються догори своєї області видимості, не зважаючи локальна вона чи глобальна





function createNewUser() {
    const firstName = prompt('Enter your first name');
    const lastName = prompt('enter your last name');
    const birthday = prompt('Enter your date of birth dd.mm.yyyy');
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        birthday: birthday,
        getLogin(){
            return `${this.firstName[0]}${this.lastName}`.toLowerCase();
        },
        getAge() {
            let date = new Date(
                this.birthday.split('.')[2],
                this.birthday.split('.')[1],
                this.birthday.split('.')[0]
            );
            const now = new Date();
            const ageNow = Math.floor((now - date) / 1000 / 60 / 60 / 24 / 365);
            return ageNow;
        },
        getPassword() {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.split('.')[2]}`;
        } 
    };
    return newUser;
}
const user1 = createNewUser();
console.log(createNewUser());
console.log(user1.getAge());
console.log(user1.getPassword());