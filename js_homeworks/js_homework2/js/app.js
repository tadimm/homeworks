//1. Які типи даних у Javascript?
// string, number, Boolean, null, NaN, underfined, symbol, object
//2. У чому різниця між == і ===?
// == - це не строге порівняння. Наприклад, якщо порівняти 1 == "1", результатом буде true, стрічку "1" буде переведено
// до number і тоді вони будуть рівні.
// === - це строге порівняння, де також будуть перевірятись типи. Наприклад 1 === "1" поверне false, тому що 1 - це number,
// а "1" - це string.
//3. Що таке оператор?
// Оператор - це внутрішня функція JavaScript. При використанні того чи іншого оператора,
// ми по суті запускаємо ту чи іншу функцію, яка виконує певні дії і повертає результат.

let firstName = prompt("What is your name?");
if (!firstName) {
  firstName = prompt("Please enter your name.");
}
let age = +prompt(`How old are you, ${firstName}?`);

if (isNaN(age)) {
  age = +prompt(`${firstName}, enter numbers here.`);
}
switch (true) {
  case age < 18:
    alert("You are not allowed to visit this website");
    break;
  case age >= 18 && age <= 22:
    let question = confirm("Are you sure you want to continue?");
    if (question) {
      alert(`Welcome, ${firstName}`);
    } else {
      alert("You are not allowed to visit this website");
    }
    break;
  case age > 22:
    alert(`Welcome, ${firstName}`);
    break;
}
