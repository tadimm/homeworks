const tabs = document.querySelector(".service-menu");
tabs.addEventListener("click", (ev) => {
  const active = document.querySelector(".active");
  active.classList.remove("active");
  ev.target.classList.add("active");

  const previousText = document.querySelector(".service-text-active");
  previousText.classList.remove("service-text-active");
  const data = ev.target.dataset.tab;
  const text = document.querySelector(`.service-text[data-tab="${data}"]`);
  text.classList.add("service-text-active");
});

const selects = document.querySelector(".amazing-work-menu");
const allPics = document.querySelectorAll(".amazing-pic");
const btn = document.querySelector(".img-load");

makeActive();

selects.addEventListener("click", (e) => {
  const shown = document.querySelector(".amazing-list-active");
  const previousPics = document.querySelectorAll(".amazing-pic-active");

  shown.classList.remove("amazing-list-active");
  e.target.classList.add("amazing-list-active");

  previousPics.forEach((li) => {
    li.classList.remove("amazing-pic-active");
  });

  if (e.target.dataset.tabName !== "") {
    makeActive();
  } else {
    allPics.forEach((li) => {
      if (!li.dataset.hasOwnProperty("loadMore")) {
        li.classList.add("amazing-pic-active");
      }
    });
  }

  btn.style.display = null;
});

btn.addEventListener("click", (ev) => {
  const activeMenuItem = document.querySelector(".amazing-list-active");
  const activeItemData = activeMenuItem.dataset.tabName;
  allPics.forEach((li) => {
    if (
      li.dataset.hasOwnProperty("loadMore") &&
      (li.dataset.tabName === activeItemData || activeItemData === "")
    ) {
      li.classList.add("amazing-pic-active");
    }
  });
  btn.style.display = "none";
});


function makeActive() {
  const activeMenuItem = document.querySelector(".amazing-list-active");
  const activeItemData = activeMenuItem.dataset.tabName;
  const currentPics = document.querySelectorAll(
    `.amazing-pic[data-tab-name="${activeItemData}"]`
  );
  currentPics.forEach((li) => {
    if (!li.dataset.hasOwnProperty("loadMore")) {
      li.classList.add("amazing-pic-active");
    }
  });
}

function slider() {
  let activeSlider = 2;
  const minImages = document.querySelectorAll(".slider-min-image");
  const sliderContents = document.querySelectorAll(".slider-content-item");
  const sliderPrevious = document.querySelector(".slider-previous");
  const sliderNext = document.querySelector(".slider-next");

  minImages.forEach((minImage, index) => {
    minImage.addEventListener("click", () => {
      minImages[activeSlider].classList.remove("active");
      sliderContents[activeSlider].classList.remove("active");
      activeSlider = index;
      sliderContents[activeSlider].classList.add("active");
      minImages[activeSlider].classList.add("active");
    });
  });

  sliderPrevious.addEventListener("click", () => {
    minImages[activeSlider].classList.remove("active");
    sliderContents[activeSlider].classList.remove("active");
    if (activeSlider === 0) {
      activeSlider = sliderContents.length - 1;
    } else {
      activeSlider--;
    }
    sliderContents[activeSlider].classList.add("active");
    minImages[activeSlider].classList.add("active");
  });

  sliderNext.addEventListener("click", () => {
    minImages[activeSlider].classList.remove("active");
    sliderContents[activeSlider].classList.remove("active");
    if (activeSlider + 1 == sliderContents.length) {
      activeSlider = 0;
    } else {
      activeSlider++;
    }
    sliderContents[activeSlider].classList.add("active");
    minImages[activeSlider].classList.add("active");
  });
}
slider();

const elem = document.querySelector(".grid");
const msnry = new Masonry(elem, {
  itemSelector: ".grid-item",
});
